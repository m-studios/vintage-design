package models

import slick.driver.PostgresDriver.api._
import play.api.db.DB
import play.api.Play.current

import scala.concurrent.Future

/**
 * Created by Sergey on 18.07.2015.
 */
object OrderMirrorDao {
  val orderMirrors = TableQuery[OrderMirrors]

  private def db: Database = Database.forDataSource(DB.getDataSource())

  def insert(orderMirror: OrderMirror): Future[Int] =
    try db.run(orderMirrors += orderMirror)
    finally db.close

  def getMirrorsOfCategory(category:String): Future[Seq[OrderMirror]] = {
    try db.run(orderMirrors.filter(_.category === category).result)
    finally db.close
  }
}
