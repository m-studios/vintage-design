package models

import slick.driver.PostgresDriver.api._
import play.api.db.DB
import play.api.Play.current
import scala.concurrent.Future

/**
 * Created by Sergey on 18.07.2015.
 */
object CategoryDao {
  val categories = TableQuery[Categories]

  private def db: Database = Database.forDataSource(DB.getDataSource())

  def insert(category: Category): Future[Int] =
    try db.run(categories += category)
    finally db.close

  def findByName(name:String):Future[Category] =
    try db.run(categories.filter(_.name === name).result.head)
    finally db.close

  def list(): Future[Seq[Category]] = {
    try {
      val query = (for {category <- categories} yield (category)).sortBy(_.id)
      db.run(query.result)
    } finally {
      db.close()
    }
  }
}
