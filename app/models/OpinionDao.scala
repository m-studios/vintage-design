package models

import slick.driver.PostgresDriver.api._
import play.api.db.DB
import play.api.Play.current
import scala.concurrent.Future

/**
 * Created by Sergey on 04.07.2015.
 */
object OpinionDao {
  val opinions = TableQuery[Opinions]

  private def db: Database = Database.forDataSource(DB.getDataSource())

  def insert(opinion: Opinion): Future[Int] =
    try db.run(opinions += opinion)
    finally db.close

  def list(): Future[Seq[Opinion]] = {
    try {
      val query = (for {opinion <- opinions} yield (opinion)).sortBy(_.name)
      db.run(query.result)
    } finally {
      db.close()
    }
  }
}
