package models

import slick.driver.PostgresDriver.api._
import play.api.db.DB
import play.api.Play.current

import scala.concurrent.Future

/**
 * Created by Sergey on 18.07.2015.
 */
object MirrorDao {

  val mirrors = TableQuery[Mirrors]

  private def db: Database = Database.forDataSource(DB.getDataSource())

  def insert(mirror: Mirror): Future[Int] =
    try db.run(mirrors += mirror)
    finally db.close

  def list(): Future[Seq[Mirror]] = {
    try {
      val query = (for {mirror <- mirrors} yield (mirror)).sortBy(_.id)
      db.run(query.result)
    } finally {
      db.close()
    }
  }

}
