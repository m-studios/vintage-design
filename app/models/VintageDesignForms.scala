package models

import play.api.data.Form
import play.api.data.Forms._

/**
 * Created by Sergey on 07.07.2015.
 */
object VintageDesignForms {

  val orderForm = Form(
    mapping(
      "name" -> nonEmptyText,
      "phone" -> nonEmptyText,
      "email" -> text,
      "text" -> nonEmptyText
    )(Order.apply)(Order.unapply)
  )

  val opinionForm = Form(
    mapping(
      "name" -> nonEmptyText,
      "text" -> nonEmptyText,
      "photo" -> nonEmptyText
  )(Opinion.apply)(Opinion.unapply)
  )
}
