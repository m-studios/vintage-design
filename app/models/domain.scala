package models

import java.util.Date

import models.Opinion
import play.api.libs.json.JsObject
import play.libs.Json
import slick.driver.PostgresDriver.api._
import slick.lifted.Tag

/**
 * Created by Sergey on 04.07.2015.
 */
case class Order(name: String, phone: String, email: String, text:String)

case class Opinion(name: String, text: String, photo: String)

class Opinions(tag: Tag) extends Table[Opinion](tag, "opinions") {

  def name = column[String]("name", O.PrimaryKey)

  def text = column[String]("text")

  def photo = column[String]("photo")

  def * = (name, text, photo) <>(Opinion.tupled, Opinion.unapply _)
}

case class Category(id:Long, name:String, description:String, photo:String)

class Categories(tag: Tag) extends Table[Category](tag, "categories") {

  def id = column[Long]("id", O.PrimaryKey)

  def name = column[String]("name")

  def photo = column[String]("photo")

  def description = column[String]("description")

  def * = (id, name, description, photo) <>(Category.tupled, Category.unapply _)
}

case class Mirror(id:Long, name:String, description:String, photos:String, price:Int)

class Mirrors(tag: Tag) extends Table[Mirror](tag, "mirrors") {

  def id = column[Long]("id", O.PrimaryKey)

  def name = column[String]("name")

  def description = column[String]("description")

  def photos = column[String]("photos")

  def price = column[Int]("price")

  def * = (id, name, description, photos, price) <>(Mirror.tupled, Mirror.unapply _)
}

case class OrderMirror(id:Long, name:String, photo:String, category:String)

class OrderMirrors(tag: Tag) extends Table[OrderMirror](tag, "orderMirrors") {

  def id = column[Long]("id", O.PrimaryKey)

  def name = column[String]("name")

  def photo = column[String]("photo")

  def category = column[String]("category")

  def * = (id, name, photo, category) <>(OrderMirror.tupled, OrderMirror.unapply _)
}
