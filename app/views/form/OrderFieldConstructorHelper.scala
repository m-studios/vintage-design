package views.form

import views.html.form.{orderFormRequiredFieldConstructor, orderFormTextAreaConstructor, orderFormFieldConstructor}
import views.html.helper.FieldConstructor


/**
 * Created by Sergey on 12.07.2015.
 */
object OrderFieldConstructorHelper {
  implicit val default = FieldConstructor(orderFormFieldConstructor.f)
  val requiredField = FieldConstructor(orderFormRequiredFieldConstructor.f)
  val textAreaConstructor = FieldConstructor(orderFormTextAreaConstructor.f)

}
