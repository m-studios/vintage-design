package controllers

import models.Order
import play.api.data.Form

/**
 * Created by Sergey on 21.07.2015.
 */
object VintageDesignFormValidator {

   def validateOrderForm(orderForm:Form[Order]):Form[Order] = {
    if (checkEmail(orderForm.get.email)) orderForm else orderForm.withError("email", "Email неправильный")
  }

  private def checkEmail(email:String): Boolean = {
    if (email == null || email.trim.length == 0)
    {
      return true
    }

    return """(?=[^\s]+)(?=(\w+)@([\w\.]+))""".r.findFirstIn(email) != None
  }
}
