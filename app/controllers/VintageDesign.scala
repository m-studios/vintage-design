package controllers

import java.util.Properties
import java.util.concurrent.TimeoutException
import javax.mail.internet.{InternetAddress, MimeMessage}
import javax.mail._

import models._
import play.api.Logger
import play.api.data.validation.{Invalid, Valid, Constraints}
import play.api.data.{validation, Form, FormError}
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.{Json, JsValue, JsObject}
import play.api.mvc.{Action, AnyContent, Controller}
import views.html

import scala.collection.mutable.ListBuffer

/**
 * Created by Sergey on 27.06.2015.
 */

object VintageDesign extends Controller {


  def index(): Action[AnyContent] = Action.async {
      implicit request => {

        val datas = for {
          categories <- CategoryDao.list()
          opinions <- OpinionDao.list()
        } yield (categories, opinions)

        datas.map{
          data => {
            val categories = data._1;
            val opinions = data._2;

            val shortDescriptions:ListBuffer[String] = new ListBuffer[String]()

            for (category <- categories)
            {
                val cutPattern = "(.*?)</cut>".r
                val withoutParagraphStarts = category.description.split("<p>").mkString("")
                val withoutParagraphEnds = withoutParagraphStarts.split("</p>").mkString("")
                val cutted = cutPattern.findFirstIn(withoutParagraphEnds).getOrElse("")
                val withotCutTag = if (cutted.length > 0) cutted.dropRight("</cut>".length) else ""
                val shortDescription = if (withotCutTag.length > 0) withotCutTag else withoutParagraphEnds
                shortDescriptions += shortDescription
            }

            Ok{html.index(categories, shortDescriptions, opinions, VintageDesignForms.orderForm)}
          }
        }
      }
  }

  def about(): Action[AnyContent] = Action { implicit request => Ok {
    html.about()
  }
  }

  def gallery(): Action[AnyContent] = Action.async {
    implicit request => CategoryDao.list().map {
      categories => Ok {
        html.gallery(categories)
      }
    }.recover {
      case ex: TimeoutException =>
        Logger.error("Problem found in gallery pictures process")
        InternalServerError(ex.getMessage)
    }
  }

  def galleryItem(categoryName: String): Action[AnyContent] = Action.async {
    implicit result => {
      val datas = for {
        category <- CategoryDao.findByName(categoryName)
        orderMirrors <- OrderMirrorDao.getMirrorsOfCategory(categoryName)
      } yield (category, orderMirrors)

      datas.map {
        data => {
          val category:Category = data._1
          val mirrors:Seq[OrderMirror] = data._2

          val pattern = "<p>(.*)</p>".r
          var descriptionsBuffer = new ListBuffer[String]()
          val matchIterator = pattern.findAllIn(category.description)
          while (matchIterator.hasNext) {
            val paragraph = matchIterator.next
            val droppedLeftParagraph = paragraph.drop("<p>".length)
            val droppedRightParagraph = droppedLeftParagraph.dropRight("/<p>".length)
            val uncutted = droppedRightParagraph.split("</cut>").mkString("")
            descriptionsBuffer += uncutted
          }

          Ok {
            html.galleryItem(category, descriptionsBuffer.toList, mirrors)
          }
        }
      }
    }
  }

  def storage(): Action[AnyContent] = Action.async {
    implicit request => MirrorDao.list().map {
      mirrors => {
        var allMirrorsPhotos = new ListBuffer[List[String]]()
        for (mirror <- mirrors) {
          val json: JsValue = Json.parse(mirror.photos)
          val photos = (json \ "photos").as[List[String]]
          allMirrorsPhotos += photos;
        }
        Ok {
          html.storage(mirrors, allMirrorsPhotos.toList, VintageDesignForms.orderForm)
        }
      }
    }.recover {
      case ex: TimeoutException =>
        Logger.error("Problem found in storage process")
        InternalServerError(ex.getMessage)
    }
  }

  def contacts(): Action[AnyContent] = Action { implicit request => Ok {
    html.contacts(VintageDesignForms.orderForm)
  }
  }

  def order(): Action[AnyContent] = Action { implicit request => Ok {
    html.order(VintageDesignForms.orderForm)
  }
  }

  def orderPost:Action[AnyContent] = Action {
    implicit request => VintageDesignForms.orderForm.bindFromRequest.fold(
      formWithErrors => {
        val validatedForm = validateEmailForm(formWithErrors.get)
        processForm(formWithErrors)
      },
      order =>
      {
        var validatedForm:Form[Order] = null

        request.body.asFormUrlEncoded.map {
          parts => {
            val nameBuffer = parts.get("name").get
            val phoneBuffer = parts.get("phone").get
            val emailBuffer = parts.get("email").get
            val textBuffer = parts.get("text").get

            val name = if (nameBuffer != null) nameBuffer(0).trim else ""
            val phone = if (nameBuffer != null) phoneBuffer(0).trim else ""
            val email = if (nameBuffer != null) emailBuffer(0).trim else ""
            val text = if (nameBuffer != null) textBuffer(0).trim else ""

            val filledOrder = new Order(name, phone, email, text)
            validatedForm = validateEmailForm(filledOrder)
          }
        }

        processForm(validatedForm)
      }
    )
  }

  def validateEmailForm(order:Order):Form[Order] = {
      val orderForm = VintageDesignForms.orderForm.fill(order)

      if (order.email != null && order.email.trim.length == 0)
      {
        return orderForm
      }

      val validationResult = Constraints.emailAddress.apply(order.email.trim)
      validationResult match {
        case Valid => orderForm
        case _ => orderForm.withError(new FormError("email", "EMAIL IS INVALID"))
      }
  }

  def processForm(form:Form[Order]) = {
    if (form.hasErrors) BadRequest(html.order(form)) else sendEmail(form.get)
  }

  def sendEmail(order: Order) = {
    val properties = new Properties()
    properties.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
    properties.put("mail.smtp.port", "587"); //TLS Port
    properties.put("mail.smtp.auth", "true"); //enable authentication
    properties.put("mail.smtp.starttls.enable", "true"); //enable START

    val username = "vintagedesignpnz@gmail.com"
    val password = "abstract1655"
    val targetEmail = "sergeyshpadyrev@gmail.com"
    val messageText = order.name +" " + order.phone + " " + (if (order.email != null) order.email else "") +" " + order.text

    val session = Session.getInstance(properties, new Authenticator {
      override def getPasswordAuthentication():PasswordAuthentication =  new PasswordAuthentication(username, password)
    })

    val message = new MimeMessage(session)
    message.setFrom(new InternetAddress(username))

    val addresses:Array[Address] = Array(InternetAddress.parse(targetEmail)(0))
    message.setRecipients(Message.RecipientType.TO, addresses)
    message.setSubject("Заказ с сайта")
    message.setText(messageText)

    Transport.send(message)

    Ok(html.order(VintageDesignForms.orderForm.fill(order), true))
  }


}
