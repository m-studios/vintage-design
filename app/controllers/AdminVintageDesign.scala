package controllers

import java.io.File
import java.util.concurrent.TimeoutException

import controllers.VintageDesign._
import models.{Opinion, OpinionDao, VintageDesignForms}
import play.api.Logger
import play.api.mvc.{AnyContent, Action}
import views.html

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by Sergey on 08.07.2015.
 */
object AdminVintageDesign {
  def testOpinion(): Action[AnyContent] = Action { implicit request => Ok {
    html.addOpinion(VintageDesignForms.opinionForm)
  }
  }

  def postOpinion = Action(parse.multipartFormData) { request =>
    request.body.file("photoUpload").map { picture =>
      val fileName = picture.filename
      val contentType = picture.contentType
      val file: File = new File("public/images/opinion/" + fileName)
      val name: String = request.body.dataParts.get("name").get(0)
      val text: String = request.body.dataParts.get("text").get(0)

      val opinion: Opinion = new Opinion(name, text, fileName)
      val insert: Future[Int] = OpinionDao.insert(opinion)
      insert.map {
        result => {

        }
      }.recover {
        case ex: TimeoutException =>
          Logger.error("Problem found in opinion add process")
          InternalServerError(ex.getMessage)
      }

      picture.ref.moveTo(file)
      Ok("File uploaded")
    }.getOrElse {
      Logger.error("Problem found in opinion add process")
      InternalServerError("Problem found in opinion add process")
    }
  }


  //  def postOpinion(): Action[AnyContent] = Action.async {
  //    implicit request => {
  //      VintageDesignForms.opinionForm.bindFromRequest.fold(
  //        formWithErrors => Future.successful(BadRequest(html.badOrder(formWithErrors.errors))),
  //        opinion => {
  //          val insert: Future[Int] = OpinionDao.insert(opinion)
  //          insert.map {
  //            result => {
  //              Redirect(routes.VintageDesign.index()).flashing("success" -> "Opinion %s has been created".format(opinion.name))
  //            }
  //          }.recover {
  //            case ex: TimeoutException =>
  //              Logger.error("Problem found in opinion add process")
  //              InternalServerError(ex.getMessage)
  //          }
  //        }
  //      )
  //  }
  //  }

}
